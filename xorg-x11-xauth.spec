Name:           xorg-x11-xauth
Version:        1.1.3
Release:        1
Epoch:          1
Summary:        X.Org X11 X authority utilities
License:        MIT-open-group
URL:            https://www.x.org
Source0:        https://www.x.org/pub/individual/app/xauth-%{version}.tar.xz

BuildRequires: gcc make
BuildRequires: pkgconfig(x11)
BuildRequires: pkgconfig(xau)
BuildRequires: pkgconfig(xext)
BuildRequires: pkgconfig(xmuu)
BuildRequires: pkgconfig(xproto) >= 7.0.17

Provides: xauth = %{version}-%{release}

%description
xauth is used to edit and display the authorization information
used in connecting to an X server.

%package_help

%prep
%autosetup -n xauth-%{version} -p1

%build
%configure
%make_build

%install
%make_install

%check
%make_build check

%files
%license COPYING
%{_bindir}/xauth

%files help
%doc README.md ChangeLog
%{_mandir}/man1/xauth.1*

%changelog
* Sat Nov 23 2024 Funda Wang <fundawang@yeah.net> - 1:1.1.3-1
- update to 1.1.3

* Thu Aug 01 2024 lingsheng <lingsheng1@h-partners.com> - 1:1.1.2-2
- Add version for provides, fix changelog bogus date

* Tue Oct 25 2022 wangkeorng <wangkerong@h-partners.com> - 1:1.1.2-1
- upgrade to 1.1.2

* Thu Jan 09 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:1.1-1
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:update version to 1.1

* Fri Oct 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:1.0.9-14
- Package init
